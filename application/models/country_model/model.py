from pydantic import BaseModel
from sqlalchemy import Column,String,Integer,BigInteger,DateTime,ForeignKey
from application.configs.database import Base,get_db
from application.models.country_model import model
from fastapi import Depends
from sqlalchemy.orm import Session,relationship
from datetime import datetime

class country(Base):
    __tablename__= 'countries'
    id=Column(Integer, primary_key=True,index=True)
    name=Column(String(25))
    cca3=Column(String(25))
    currency_code=Column(String(25))
    currency=Column(String(25))
    capital=Column(String(25))
    region=Column(String(25))
    subregion=Column(String)
    area=Column(BigInteger)
    map_url=Column(String) 
    population=Column(BigInteger)
    flag_url=Column(String)
    created_at=Column(DateTime,default=datetime.utcnow)
    updated_at=Column(DateTime,default=datetime.utcnow, onupdate=datetime.utcnow)
    # neighbour_country_id = Column(Integer,ForeignKey('neighbouring_country.id')) 
    
    # padosi=relationship("Neighbours",back_populates="items")
    

    @classmethod
    def get_all_country(db):
        all_countries= db.query(model.country).all()
        return all_countries
     
    
    @classmethod
    def search_items(q,db:Session=Depends(get_db)):
        return db.query(model.country).filter(model.country.name.ilike(f"%{q}%")).all()


class Neighbours(Base):
    __tablename__= 'neighbouring_country'
    name=Column(String(25)) 
    id=Column(Integer,primary_key=True,index=True)
    cca3=Column(String(25))
    currency_code=Column(String(25))
    currency=Column(String(25))
    capital=Column(String(25))
    region=Column(String(25))
    subregion=Column(String) 
    area=Column(BigInteger)
    map_url=Column(String) 
    population=Column(BigInteger)
    flag_url=Column(String)
    created_at=Column(DateTime,default=datetime.utcnow)
    updated_at=Column(DateTime,default=datetime.utcnow, onupdate=datetime.utcnow)
    # items=relationship("Country",back_populates="padosi")

    # foreign_id = Column(Integer, ForeignKey("countries.id"))
    
    # items=relationship("Country",back_populates="neighbouring_country")
     
