from fastapi import FastAPI
from application.models.country_model import model
from application.configs.database import engine
from application.routes import country_assignment,neighbour_country

app=FastAPI()

model.Base.metadata.create_all(bind=engine)

app.include_router(country_assignment.route)
app.include_router(neighbour_country.route)

@app.get('/helo')
def hello_world():
    return {'data':"Hello Nikhil"}