from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker   
from fastapi import Depends   

from sqlalchemy.orm import declarative_base,sessionmaker
from sqlalchemy import create_engine


SQLALCHEMY_DATABASE_URL = 'sqlite:///./country.db'


engine =create_engine(SQLALCHEMY_DATABASE_URL,connect_args={"check_same_thread":False} )
# engine =create_engine("postgresql://nikhilkumar783871:nikhil2211@localhost/demo_db",echo=True)

SessionLocal=sessionmaker(bind=engine,autocommit=False,autoflush=False)

Base=declarative_base()

def get_db():
    db=SessionLocal()
    try:
        yield db
    finally:
        db.close()
        
# activeSession = Depends(get_db)

        
    