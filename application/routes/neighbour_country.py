from fastapi import APIRouter,Depends,Response,Request,Query,requests
from typing import Optional
from sqlalchemy.orm import Session
from application.configs.database import get_db
from application.models.country_model import model
# from application.utilities.flask import APIResponse,APIError
from application.utilities.serialization import serialize
from fastapi.responses import JSONResponse
from application.schemas import neighbour_country
from datetime import datetime



route=APIRouter(
    tags=['Neighbour Country']
    )

@route.post('/neighbourcountry')
def add_neighbour(request:neighbour_country.NeighbourCountry,db:Session=Depends(get_db)):
    # obj=db.query(model.country).filter(model.country.id==id).first()
    new_country=model.Neighbours(name=request.name,cca3=request.cca3,currency_code=request.currency_code,currency=request.currency,capital=request.capital,region=request.region,subregion=request.subregion,area=request.area,map_url=request.map_url,population=request.population,flag_url=request.flag_url)
    db.add(new_country)
    db.commit()
    # db.refresh()
    return new_country

@route.get('/country/{id}/neighbour')
def get_by_id(id,db:Session=Depends(get_db)):
    obj=db.query(model.Neighbours).filter(model.Neighbours.id==id).first()
    # return JSONResponse(content={"message":"Country list","Data": serialize(obj)}, status_code=200)    
    if not obj:
        return JSONResponse(content={"message":"Country not found","Data": serialize(obj)}, status_code=400)    

    else:
        return JSONResponse(content={"message":"Country Neighbour list","Data": serialize(obj)}, status_code=200) 


    
