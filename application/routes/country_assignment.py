from fastapi import APIRouter,Depends,Response,Request,Query
from typing import Optional
from sqlalchemy.orm import Session
from application.configs.database import get_db
from application.models.country_model import model
# from application.utilities.flask import APIResponse,APIError
from application.utilities.serialization import serialize
from fastapi.responses import JSONResponse


route=APIRouter(
    tags=['Country Assignment']
    )


@route.get('/countryyy')
def get_all(db:Session=Depends(get_db)):
    obj=db.query(model.country).all()
    # obj=model.country.get_all_country(db)
    # return obj
    # return APIResponse(message="admindeleted",request=request),data=serialize(obj)).result()
    return JSONResponse(content={"message":"Country list","Data": serialize(obj)}, status_code=200)    

@route.get('/country/{id}')
def get_by_id(id,db:Session=Depends(get_db)):
    obj=db.query(model.country).filter(model.country.id==id).first()
    # return JSONResponse(content={"message":"Country list","Data": serialize(obj)}, status_code=200)    
    if not obj:
        return JSONResponse(content={"message":"Country not found","Data": serialize(obj)}, status_code=400)    

    else:
        return JSONResponse(content={"message":"Country list","Data": serialize(obj)}, status_code=200)    

@route.get("/paginate")
def get_paginated_list(page: Optional[int] = Query(1, ge=1), page_size: Optional[int] = Query(10, ge=1, le=100), db: Session = Depends(get_db)):
    """
    Returns a list of items, paginated by the provided page and page_size parameters.
    """
    offset = (page - 1) * page_size
    items= db.query(model.country).offset(offset).limit(page_size).all()
    
    return JSONResponse(content={"message":"Country list","Data": serialize(items),"page":page,"per_page":page_size,"total":len(items)}, status_code=200) 




@route.get('/sort')  
def get_sorted_countries(sort_by: Optional[str] = Query('name', regex='^(name|population|area)$'), order: Optional[str] = Query('asc', regex='^(asc|desc)$'), db: Session = Depends(get_db)):
    
    def get_countries_sorted(db: Session, sort_by: str = 'name', order: str = 'asc') -> list[model.country]:
       
        sort_column = getattr(model.country, sort_by)
        if order == 'asc':
            return db.query(model.country).order_by(sort_column.asc()).all()
        elif order == 'desc':
            return db.query(model.country).order_by(sort_column.desc()).all()
        elif order == 'pop_asc' and sort_by == 'population':
            return db.query(model.country).order_by(sort_column.asc()).all()
        elif order == 'pop_desc' and sort_by == 'population':
            return db.query(model.country).order_by(sort_column.desc()).all()
        elif order == 'name_asc' and sort_by == 'name':
            return db.query(model.country).order_by(sort_column.asc()).all()
        elif order == 'name_desc' and sort_by == 'name':
            return db.query(model.country).order_by(sort_column.desc()).all()

        elif order == 'area_asc' and sort_by == 'area':
            return db.query(model.country).order_by(sort_column.asc()).all()
        elif order == 'area_desc' and sort_by == 'area':
            return db.query(model.country).order_by(sort_column.desc()).all()

        else:
            return db.query(model.country).all()
    countries =get_countries_sorted(db, sort_by=sort_by, order=order)
    return countries



@route.get('/search/name')
def search_country(q: Optional[str] = Query(None, min_length=3, max_length=50), db:Session=Depends(get_db)):
    if q:
        items = db.query(model.country).filter(model.country.name.ilike(f"%{q}%")).all()
    else:
        items = db.query(model.country).all()
    
    return items    

@route.get('/search/region')
def search_country(q: Optional[str] = Query(None, min_length=3, max_length=50), db:Session=Depends(get_db)):
    if q:
        items = db.query(model.country).filter(model.country.region.ilike(f"%{q}%")).all()
    else:
        items = db.query(model.country).all()
    
    return items  


@route.get('/search/subregion')
def search_country(q: Optional[str] = Query(None, min_length=3, max_length=50), db:Session=Depends(get_db)):
    if q:
        items = db.query(model.country).filter(model.country.subregion.ilike(f"%{q}%")).all()
    else:
        items = db.query(model.country).all()
    
    return items 





