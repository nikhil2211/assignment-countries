from pydantic import BaseModel

class NeighbourCountry(BaseModel):
    name:str
    cca3:str
    currency_code:str
    currency:str
    capital:str
    region:str
    subregion:str
    area:float
    map_url:str
    population:int
    flag_url:str